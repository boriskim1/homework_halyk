from PyPDF4 import PdfFileReader


with open('boris.pdf', 'rb') as book:
    PDF_read = PdfFileReader(book)
    for i in range(0, PDF_read.getNumPages()):
        print(PDF_read.getPage(i).extractText())